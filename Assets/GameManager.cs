﻿using System;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor.iOS.Xcode;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;


    public float timer = 60;
    
    public int score, lessScore;
    public Text showScore;
    public Text showTime, readyText, timeUp, finalScore, scoreToRecover;
    public bool onTime = true;//Si es falso, el juego acaba
    public bool ready = false;
    public bool multiply, bonusOnScene, isPaused, wasHit;
    public Animator plusTwo, plusRecover, plusTime;
    public GameObject byTwo, restart, pauseButton, continueButton;
    public FloatingJoystick Joystick;

    private void Awake()
    {
        if (instance == null)
        {

            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }

        showScore.text = "Score: " + score;
        StartCoroutine("readyToPlay");
      

        plusTwo.GetComponent<Animator>();
        plusRecover.GetComponent<Animator>();
        plusTime.GetComponent<Animator>();

        timer = 60;
        score = 0;
        lessScore = 50;

        ready = false;
        onTime = true;
    }


    
    void Start()
    {
        InvokeRepeating("evenLessPoints", 10f, 10f);
    }

    


    private void Update()
    {
        if (onTime && ready)
        {
            timer -= Time.deltaTime;
            showTime.text = "Time Left: " + (timer).ToString("0");
            score += Mathf.FloorToInt(Time.deltaTime);
            showScore.text = "Score: " + score;

        }

        if (timer <= 0)//Deshabilitamos todo excepto el restart
        {
            timer = 0;
            
            onTime = false;
            pauseButton.SetActive(false);
            Joystick.enabled = false;
            timeUp.gameObject.SetActive(true);
            finalScore.gameObject.SetActive(true);
            finalScore.text = " Your score: " + score;
            restart.SetActive(true);

           
        }

        if(score < 0)
        {
            score = 0;
        }

        if (multiply)
        {
            showScore.color = new Color(0, 0, 255);
            StartCoroutine(disableMultiply());
        }

        /*if (wasHit)
        {
            StartCoroutine(disableHit());
        }*/

       

    }


    public void AddScore(int scoreAdd)
    {
        if (!multiply)
        {
            score += scoreAdd;
            showScore.text = "Score: " + score;
        } else
        {
            score += scoreAdd * 2;
            showScore.text = "Score: " + score;
        }
        
    }

    public void minusScore()
    {
        score -= lessScore;
    }

    void evenLessPoints()
    {
        lessScore += 5;
    }

    public void recoverScore()
    {
        if (wasHit)
        {
            score += lessScore;
            scoreToRecover.text = "+ " + lessScore;
            plusRecover.SetTrigger("Active");
        }
        else if(!wasHit)
        {
            score += 150;
            scoreToRecover.text = "+ 150 ";
            plusRecover.SetTrigger("Active");
        }
        

    }

    public void PauseGame()
    {
        isPaused = true;
        pauseButton.SetActive(false);
        continueButton.SetActive(true);
        Joystick.enabled = false;
        Time.timeScale = 0;
    }

    public void ContinueGame()
    {
        StartCoroutine(UnPaused());
        
    }
    public void AddTime(float addTime)
    {
        timer += addTime;
        plusTime.SetTrigger("Active");

    }

    public void PlayerHit()
    {
        wasHit = true;

       
        
        StopCoroutine("disableHit");
        StartCoroutine("disableHit");
    }


    IEnumerator readyToPlay()
    {

        readyText.text = "Are";
        yield return new WaitForSeconds(0.5f);
        readyText.text = "You";
        yield return new WaitForSeconds(0.5f);
        readyText.text = "Ready?";
        yield return new WaitForSeconds(0.9f);
        ready = true;
        readyText.text = " GO!";
        yield return new WaitForSeconds(0.8f);
        readyText.text = "";
        pauseButton.SetActive(true);


    }

    IEnumerator UnPaused()
    {
        continueButton.SetActive(false);
        readyText.text = "Continue in 3";
        yield return new WaitForSecondsRealtime(1f);
        readyText.text = "Continue in 2";
        yield return new WaitForSecondsRealtime(1f);
        readyText.text = "Continue in 1";
        yield return new WaitForSecondsRealtime(1f);
        readyText.text = "GO!";
        yield return new WaitForSecondsRealtime(0.5f);
        readyText.text = "";
        isPaused = false;
        pauseButton.SetActive(true);
        
        Joystick.enabled = true;
        Time.timeScale = 1f;
    }
    

    IEnumerator disableMultiply()
    {
        byTwo.SetActive(true);
        yield return new WaitForSeconds(5f);
        plusTwo.SetTrigger("Active");
        multiply = false;
        showScore.color = new Color(255, 255, 255);
        byTwo.SetActive(false);
    }

    IEnumerator disableHit()
    {

        while (true)
        {
            yield return new WaitForSeconds(5f);
            wasHit = false;
        } 
       
    }

   

}
