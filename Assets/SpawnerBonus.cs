﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnerBonus : MonoBehaviour
{

    public GameObject bonusObj; 
    public GameObject  timebonus, recoverBonus, doubleBonus;
    public float newPositionX;
    public float newPositionY;
    int definernumber;
    
    
    public static int objSpawned;

    

    private void Awake()
    {
        objSpawned = 0;
        InvokeRepeating("SpawnObject", 2.5f, 1.5f);
        InvokeRepeating("SpawnObject2", 13f, 13f);
        InvokeRepeating("SpawnObject3", 19f, 19f);
    }
    

   
    void Update()
    {

        

        if (GameManager.instance.ready)
        {
            newPositionX = Random.Range(-2.4f, 2.4f);
            newPositionY = Random.Range(-0.24f, 4f);
            StartCoroutine(Position());
        } 
        
        if(GameManager.instance.timer <= 0)
        {
            StopAllCoroutines();
            CancelInvoke("SpawnObject");
            CancelInvoke("SpawnObject2");
            this.gameObject.SetActive(false);
        }


        if(objSpawned >= 5)
        {
            objSpawned = 4;
            Debug.Log("Limite");
        }
        
        if(objSpawned < 0)
        {
            objSpawned = 0;
        }

       
    }

    IEnumerator Position()
    {
        yield return new WaitForSeconds(0.5f);
        transform.position = new Vector2(newPositionX, newPositionY);
    }

    void SpawnObject()
    {
       
            Instantiate(bonusObj, transform.position, transform.rotation);
            if (objSpawned < 5)
            {
                objSpawned += 1;
                Debug.Log("objSpawn " + objSpawned);
            }
        
       
        

    }
    void SpawnObject2()
    {
        if (!GameManager.instance.bonusOnScene)
        {
            Instantiate(timebonus, transform.position, transform.rotation);
        }
        

    }

    void SpawnObject3()
    {
        if (!GameManager.instance.bonusOnScene)
        {
            definernumber = Random.Range(0, 10);
            Debug.Log(definernumber);
            if(definernumber >= 0 &&  definernumber <5)
            {
                Instantiate(doubleBonus, transform.position, transform.rotation);
            } else if (definernumber >= 5 && definernumber <= 10)
            {
                Instantiate(recoverBonus, transform.position, transform.rotation);
            }
           
        }
       
    }


    void RestartInvoke()
    {
        InvokeRepeating("SpawnObject", 4.5f, 4.5f);
        
    }

}
