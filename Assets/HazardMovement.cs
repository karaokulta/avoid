﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardMovement : MonoBehaviour
{

    public float movespeed;
    public float upVelocity;
    public Rigidbody2D rb;
    public float multiplier;
    private float dirX;
    private float dirY;
    public int lessPoints = -1;
    public float left, right, top, bottom;
    

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        InvokeRepeating("moreIntensity", 10f, 10f);

    }


    private void FixedUpdate()
    {
        dirX = movespeed;
        dirY = upVelocity;

        if (GameManager.instance.ready)
        {
            rb.velocity = new Vector2(dirX * multiplier, dirY * multiplier);
            rb.gravityScale = 1f;
        } else
        {
            rb.velocity = Vector2.zero;
            rb.gravityScale = 0;
        }

        if(GameManager.instance.timer <= 0)
        {
            rb.velocity = Vector2.zero;
            rb.gravityScale = 0;
        }
        
    }

    void moreIntensity()
    {
        multiplier += 0.04f;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 newPos = transform.position;

        if (collision.gameObject.CompareTag("LeftBorder"))
        {

            
            
            newPos.x = right;
            multiplier += 0.001f;
            
        }
        if (collision.gameObject.CompareTag("RightBorder"))
        {

        

            newPos.x = left;
            multiplier += 0.001f;
        }

        if (collision.gameObject.CompareTag("TopBorder"))
        {

            
            newPos.y = bottom;
          
            multiplier += 0.001f;
        }

        if (collision.gameObject.CompareTag("BottomBorder"))
        {


            newPos.y = top;
         
            multiplier += 0.001f;
        }

        if (collision.gameObject.CompareTag("Hazard") || collision.gameObject.CompareTag("Player"))
        {
            multiplier += 0.001f;
            upVelocity *= -1f;
            movespeed *= -1f;
        }

        transform.position = newPos;
    }

}

