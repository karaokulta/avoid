﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{

    public float moveSpeed;
    float horizontalMove;
    float verticalMove;
    public Rigidbody2D rb;
    public Joystick joystick;
    
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    
    void Update()
    {


        horizontalMove = joystick.Horizontal;
        verticalMove = joystick.Vertical;

        if (GameManager.instance.timer > 0 && GameManager.instance.ready)
        {

            if (!GameManager.instance.isPaused)
            {
                if (joystick.Horizontal == 0 && joystick.Vertical == 0)
                {
                    rb.AddForce(new Vector2(0.009f, 0.009f));
                }
                else
                {
                    rb.velocity = new Vector3(horizontalMove, verticalMove) * moveSpeed;
                }

            }
            

        } else if(GameManager.instance.timer <= 0)
        {
            rb.velocity = Vector2.zero;
        }
        
    }
}
