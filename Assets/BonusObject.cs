﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusObject : MonoBehaviour
{
   
    public int bonus;
   
    private void Awake()
    {
        
        if (SpawnerBonus.objSpawned >= 5)
        {
            this.gameObject.SetActive(false);
        }
    }
    void Start()
    {
        

        if (SpawnerBonus.objSpawned >= 5)
        {
            this.gameObject.SetActive(false);
        }

    }

    void Update()
    {
        if(GameManager.instance.timer <= 0)
        {
            this.gameObject.SetActive(false);
        }
    }

  
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

           
                GameManager.instance.AddScore(bonus);
                Debug.Log("Colision");
                SpawnerBonus.objSpawned -= 1;
                this.gameObject.SetActive(false);

        }
    }

}

